package cz.httptechwork_dev.taxi;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class CarListActivity extends ActionBarActivity {
String city = "Opa";
    ArrayList<car> itemlist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);


        getData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_car_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
    public void getData()
    {
        Intent intent = getIntent();
        city = intent.getStringExtra("city");

        StrictMode.ThreadPolicy policy = new
            StrictMode.ThreadPolicy.Builder()
            .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String result = dbConnection.execute("SELECT x.id,x.name,x.company,x.city,x.stars,y.tax from zzcar x,zzcomp y where x.free='1' and x.city ='"+city+"' and y.name = x.company");


        try {

            JSONArray jArray = new JSONArray(result);
            for (int i = 0; i < jArray.length(); i++) {

                JSONObject json = jArray.getJSONObject(i);
                itemlist.add(new car(json.getString("id"), json.getString("name"), json.getString("company"), json.getString("city"), json.getString("stars"), json.getString("tax")));

            }
        } catch (Exception e) {
            Log.e("log_tag", "Error Parsing Data (getting items info)" + e.toString());
        }

        final ListView listview = (ListView) findViewById(R.id.listView);
        final  MySimpleArrayAdapter adapter = new  MySimpleArrayAdapter(this,itemlist);
        listview.setAdapter(adapter);
    }
}

