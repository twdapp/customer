package cz.httptechwork_dev.taxi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
    public void select_time (View v)
    {

        Context context = getApplicationContext();
        CharSequence text = "Time picker";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);

        toast.show();

        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");

        TextView time = (TextView) findViewById(R.id.timeWiew);
        time.setText(newFragment.getH()+":"+newFragment.getM());
    }

    public void next (View v)
    {TextView city = (TextView) findViewById(R.id.CityText);
        TextView from = (TextView) findViewById(R.id.FromText);
        TextView to = (TextView) findViewById(R.id.ToText);
        TextView time = (TextView) findViewById(R.id.timeWiew);

        Intent intent = new Intent(this, CarListActivity.class);
        intent.putExtra("city",city.getText());
        intent.putExtra("from",from.getText());
        intent.putExtra("to",to.getText());
        intent.putExtra("time",time.getText());
        startActivity(intent);


    }
}

