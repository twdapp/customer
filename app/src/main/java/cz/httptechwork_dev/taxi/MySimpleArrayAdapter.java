package cz.httptechwork_dev.taxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MySimpleArrayAdapter extends ArrayAdapter<car> {
    private final Context context;
    private final ArrayList<car> values;

    public MySimpleArrayAdapter(Context context, ArrayList<car> values) {
        super(context, R.layout.lineview, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.lineview, parent, false);
        RatingBar id = (RatingBar) rowView.findViewById(R.id.Rating);
        TextView name = (TextView) rowView.findViewById(R.id.Name);
        TextView company = (TextView) rowView.findViewById(R.id.Company);
        TextView cost = (TextView) rowView.findViewById(R.id.Cost);


        car help = values.get(position);
        id.setRating(Float.parseFloat(help.getStar()));
        name.setText(help.getName());
       cost.setText(help.getCost());
        company.setText(help.getCompany());
        return rowView;
    }
}
