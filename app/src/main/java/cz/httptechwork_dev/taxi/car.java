package cz.httptechwork_dev.taxi;

/**
 * Created by Nariel on 1. 6. 2016.
 */
public class car {

    private String id;
    private String name;
    private String company;
    private String city;
    private String star;
    private String cost;

    public car(String id, String name, String company, String city, String star, String cost){
        this.setId(id);
        this.setName(name);
        this.setCompany(company);
        this.setCity(city);
        this.setStar(star);
        this.setCost(cost);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
