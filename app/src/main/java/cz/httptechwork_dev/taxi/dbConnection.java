package cz.httptechwork_dev.taxi;

        import android.os.AsyncTask;
        import android.util.Log;

        import org.apache.http.HttpEntity;
        import org.apache.http.HttpResponse;
        import org.apache.http.NameValuePair;
        import org.apache.http.auth.AuthScope;
        import org.apache.http.auth.UsernamePasswordCredentials;

        import org.apache.http.client.entity.UrlEncodedFormEntity;
        import org.apache.http.client.methods.HttpPost;

        import org.apache.http.impl.client.DefaultHttpClient;
        import org.apache.http.message.BasicNameValuePair;


        import java.io.BufferedReader;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.util.ArrayList;
        import java.util.List;

/**
 * Created by Vozna on 25.3.2015.
 */
public class dbConnection extends AsyncTask<String, Void, String> {

    protected String doInBackground(String... urls) {

        return "na na na Batman";
    }

    public static String execute(String MysqlQ) {
        String result = "";
        InputStream isr = null;

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            httpClient.getCredentialsProvider().setCredentials(
                    AuthScope.ANY,
                    new UsernamePasswordCredentials("theTom", "theTom69*"));
            String url = "http://techwork-dev.cz/techlab/app/php/select.php";

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("query", MysqlQ));
            Log.e("Q",MysqlQ);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity entity = httpResponse.getEntity();

            isr = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
            return "Error in http connection ";
        }
//convert response to string

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(isr, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            isr.close();
            result = sb.toString();
            int ind = result.indexOf("[");
            if(ind>=0) result = result.substring(ind);
        } catch (Exception e) {
            Log.e("log_tag", "Error  converting result " + e.toString());
            return "Error  converting result";
        }

        return result;
    }
}
